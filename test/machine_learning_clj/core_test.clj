(ns machine-learning-clj.core-test
  (:require [clojure.test :refer :all]
            [machine-learning-clj.core :refer :all]
            [neuralnetworks.core :as nn]
            [clojure.data.csv :as csv])
  ;[incanter.core :refer :all]
  ;[incanter.stats :refer :all]
  ;[incanter.charts :refer :all]

  (:import (org.encog.neural.networks BasicNetwork)
           (org.encog.neural.networks.layers BasicLayer)
           (org.encog.engine.network.activation ActivationSigmoid)
           (org.encog.ml.data.basic BasicMLDataSet)
           (org.encog.neural.networks.training.propagation.resilient ResilientPropagation)
           (org.encog.ml.data MLDataPair MLDataSet)
           (org.encog.ml.data.versatile.sources CSVDataSource VersatileDataSource)
           (org.encog.util.csv CSVFormat)
           (org.encog.ml.data.versatile.columns ColumnType)
           (org.encog.ml.data.versatile VersatileMLDataSet)))

(deftest basic-xor-nn
  (def xor-input (into-array (map double-array [[0.0, 0.0]
                                                [1.0, 0.0]
                                                [0.0, 1.0]
                                                [1.0, 1.0]])))
  (def xor-ideal (into-array (map double-array [[0.0]
                                                [1.0]
                                                [1.0]
                                                [0.0]])))

  (def network (new BasicNetwork))
  (.addLayer network (new BasicLayer nil true 2))
  (.addLayer network (new BasicLayer (new ActivationSigmoid) true 3))
  (.addLayer network (new BasicLayer (new ActivationSigmoid) false 1))
  (.finalizeStructure (.getStructure network))
  (.reset network)

  (def training-set (new BasicMLDataSet xor-input xor-ideal))
  (def train (new ResilientPropagation network training-set))

  (def epoch (atom 0))
  (.iteration train)
  (while (> (.getError train) 0.000001)
    (prn (deref epoch) " - error=" (.getError train))
    (swap! epoch inc)
    (.iteration train))

  (.finishTraining train)

  ; test neural
  (prn "Results")
  (doseq [pair training-set]
    (do
      (let [data-pair (.getInput ^MLDataPair pair)
            output (.compute network data-pair)]
        (prn "input=" (.getData data-pair 0) ", " (.getData data-pair 1))
        (prn "output=" (.getData output 0) ", ideal=" (.getData (.getIdeal pair) 0))))))

(deftest basic-classification
  (let [iris-file (clojure.java.io/as-file "test/resources/iris.csv")
        csv-data-source (new CSVDataSource iris-file false CSVFormat/DECIMAL_POINT)
        data (new VersatileMLDataSet csv-data-source)
        _ (.defineSourceColumn data "sepal-length" 0 ColumnType/continuous)
        _ (.defineSourceColumn data "sepal-width" 1 ColumnType/continuous)
        _ (.defineSourceColumn data "petal-length" 2 ColumnType/continuous)
        _ (.defineSourceColumn data "petal-length" 3 ColumnType/continuous)
        output-column (.defineSourceColumn data "species" 4 ColumnType/nominal)]

    (.analyze data)))


(deftest nn-classification
  (let [iris-file (clojure.java.io/reader "test/resources/iris.csv")
        shuffled-input (shuffle (csv/read-csv iris-file))
        ; output-map {"iris-versicolor" 0 "iris-setosa" 1 "iris-virginica" 2}
        output-map (->> (map #(first (rseq %)) shuffled-input)
                        set
                        vec
                        (map-indexed #(vec [%2 %1]))
                        (into {}))

        training-count (int (Math/floor (* 0.6 (count shuffled-input))))
        cross-validation-count (int (Math/floor (* 0.2 (count shuffled-input))))
        test-count (- (count shuffled-input) training-count cross-validation-count)
        output-features-count (count output-map)

        input-partitioned-by-output (map #(partition-by (fn [value] (contains? output-map value)) %)
                                         shuffled-input)

        zeroes (vec (repeat output-features-count 0.0))

        ; input and output matrix
        input-matrix (->> input-partitioned-by-output
                          (map first)
                          (mapv #(map (fn [value] (Double/parseDouble value)) %)))
        output-matrix (->> input-partitioned-by-output
                           (map second)
                           (map #(map (fn [value]
                                        (let [index (get output-map value)]
                                          (assoc zeroes index 1.0))) %))
                           (mapv flatten))]



    ; todo train and test
    (prn input-matrix)
    (prn output-matrix)
    (prn training-count cross-validation-count test-count)))



