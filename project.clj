(defproject machine-learning-clj "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :java-source-paths ["src/java"]
  :dependencies [[org.clojure/clojure "1.8.0"]

                 ; machine learning
                 [org.encog/encog-core "3.3.0"]

                 ; statistical / chart
                 [incanter/incanter-core "1.5.7"]
                 [incanter/incanter-charts "1.5.7"]
                 [incanter/incanter-io "1.5.7"]

                 ; nn
                 [ronaldsuwandi/neuralnetworks "0.1.1-SNAPSHOT"]

                 [org.clojure/data.csv "0.1.3"]

                 ;matrix
                 ;[uncomplicate/neanderthal "0.5.0"]
                 ;[uncomplicate/clojurecl "0.5.0"]
                 [net.mikera/core.matrix "0.51.0"]
                 [net.mikera/vectorz-clj "0.43.1"]])

