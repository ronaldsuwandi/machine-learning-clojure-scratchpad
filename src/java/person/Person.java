package person;

public class Person {
    String name;
    public Person() {
        name = "default";
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "name->" + name;
    }
}
